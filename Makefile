
SRC		:= $(wildcard ./src/*.c)
BUILDPATH := ./build
LIB		:= -lOpenCL -lGAL
TARGET	:= test

CC		:= gcc
LD		:= gcc
RM		:= rm -rf


$(TARGET): $(SRC)
	$(CC) -o $(BUILDPATH)/$(TARGET) $(SRC) $(LIB)

clean:
	$(RM) $(TARGET)

