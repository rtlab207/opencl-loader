#include "mycl.h"
#include <stdlib.h>
#include <stdio.h>

void openclCheckError(cl_int errNum, char *symbol, int extra);

int main()
{
	POCL_Init();

	cl_int errNum;
	cl_uint gnumPlatforms, cnumPlatforms;
	cl_platform_id *gplatformIDs, *cplatformIDs;

	size_t size;

	errNum = clGetPlatformIDs(0, NULL, &gnumPlatforms);
	openclCheckError(errNum, "clGetPlatformIDs", 1);

	gplatformIDs = (cl_platform_id*)malloc(sizeof(cl_platform_id)*gnumPlatforms);

	errNum = clGetPlatformIDs(gnumPlatforms, gplatformIDs, NULL);
	openclCheckError(errNum, "clGetPlatformIDs", 2);

	errNum = clGetPlatformInfo(gplatformIDs[0], CL_PLATFORM_NAME, 0, NULL, &size);
	openclCheckError(errNum, "clGetPlatformInfo", 2);

	char *gname = (char*)malloc(sizeof(char)*size);

	errNum = clGetPlatformInfo(gplatformIDs[0], CL_PLATFORM_NAME, size, gname, NULL);
	openclCheckError(errNum, "clGetPlatformInfo", 2);

	printf("Platform id = %d, name = %s\n", gplatformIDs[0], gname);

////////////////////////////////////////////////////////////////

	errNum = cpuclGetPlatformIDs(0, NULL, &cnumPlatforms);
	openclCheckError(errNum, "cpuclGetPlatformIDs", 1);

	cplatformIDs = (cl_platform_id*)malloc(sizeof(cl_platform_id)*cnumPlatforms);

	errNum = cpuclGetPlatformIDs(cnumPlatforms, cplatformIDs, NULL);
	openclCheckError(errNum, "cpuclGetPlatformIDs", 2);

	errNum = cpuclGetPlatformInfo(cplatformIDs[0], CL_PLATFORM_NAME, 0, NULL, &size);
	openclCheckError(errNum, "cpuclGetPlatformInfo", 2);

	char *cname = (char*)malloc(sizeof(char)*size);

	errNum = cpuclGetPlatformInfo(cplatformIDs[0], CL_PLATFORM_NAME, size, cname, NULL);
	openclCheckError(errNum, "clGetPlatformInfo", 2);

	printf("Platform id = %d, name = %s\n", cplatformIDs[0], cname);
	return 0;
}

void openclCheckError(cl_int errNum, char *symbol, int extra)
{
	if(errNum != CL_SUCCESS){
		fprintf(stderr, "error: %s(%d) %d\n", symbol, errNum, extra);
	}
}

