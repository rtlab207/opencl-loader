#include <CL/cl.h>

extern void POCL_Init();

extern cl_int (*cpuclGetPlatformIDs)(cl_uint, cl_platform_id*, cl_uint*);
extern cl_int (*cpuclGetPlatformInfo)(cl_platform_id, cl_platform_info, size_t, void*, size_t*);
extern cl_int (*cpuclGetDeviceIDs)(cl_platform_id, cl_device_type, cl_uint, cl_device_id, cl_uint);
extern cl_int (*cpuclGetDeviceInfo)(cl_device_id, cl_device_info, size_t, void*, size_t*);
extern cl_context (*cpuclCreateContext)(const cl_context_properties*, cl_uint, const cl_device_id*, void (CL_CALLBACK *)(const char*, const void*, size_t, void*), void*, cl_int*);
extern cl_context (*cpuclCreateConstexFromType)(const cl_context_properties*, cl_device_type, void (CL_CALLBACK *)(const char*, const void*, size_t, void*), void*, cl_int*);
extern cl_int (*cpuclGetContextInfo)(cl_context, cl_context_info, size_t, void*, size_t*);
extern cl_int (*cpuclRetainContext)(cl_context);
extern cl_int (*cpuclReleaseContext)(cl_context);
extern cl_int (*cpuclReleaseCommandQueue)(cl_command_queue);
extern cl_int (*cpuclReleaseMemObject)(cl_mem);
extern cl_int (*cpuclReleaseKernel)(cl_kernel);
extern cl_int (*cpuclReleaseProgram)(cl_program);
extern cl_program (*cpuclCreateProgramWithSource)(cl_context, cl_uint, const char**, const size_t*, cl_int*);
extern cl_int (*cpuclBuildProgram)(cl_program, cl_uint, const cl_device_id*, const char*, void (CL_CALLBACK *)(cl_program, void*), void*);
extern cl_int (*cpuclGetProgramBuildInfo)(cl_program, cl_device_id, cl_program_build_info, size_t, void*, size_t*);
extern cl_command_queue (*cpuclCreateCommandQueue)(cl_context, cl_device_id, cl_command_queue_properties, cl_int*);
extern cl_kernel (*cpuclCreateKernel)(cl_program, const char*, cl_int*);
extern cl_mem (*cpuclCreateBuffer)(cl_context, cl_mem_flags, size_t, void*, cl_int);
extern cl_int (*cpuclEnqueueWriteBuffer)(cl_command_queue, cl_mem, cl_bool, size_t, size_t, void*, cl_uint, const cl_event*, cl_event*);
extern cl_int (*cpuclWaitForEvents)(cl_uint, const cl_event*);
extern cl_int (*cpuclSetKernelArg)(cl_kernel, cl_uint, size_t*, const void*);
extern cl_int (*cpuclEnqueueNDRangeKernel)(cl_command_queue, cl_kernel, cl_uint, const size_t*, const size_t*, const size_t*, cl_uint, const cl_event*, cl_event*);
extern cl_int (*cpuclGetEventProfilingInfo)(cl_event, cl_profiling_info, size_t, void*, size_t*);

