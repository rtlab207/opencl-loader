#include "mycl.h"
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

#define imx6 1 
#define CCL_PATH "/usr/local/lib/libpocl.so"
#define GCL_PATH "/usr/lib/libOpenCL.so"
#define GAL_PATH "/usr/lib/libGAL.so"

cl_int (*cpuclGetPlatformIDs)(cl_uint, cl_platform_id*, cl_uint*);
cl_int (*cpuclGetPlatformInfo)(cl_platform_id, cl_platform_info, size_t, void*, size_t*);
cl_int (*cpuclGetDeviceIDs)(cl_platform_id, cl_device_type, cl_uint, cl_device_id, cl_uint);
cl_int (*cpuclGetDeviceInfo)(cl_device_id, cl_device_info, size_t, void*, size_t*);
cl_context (*cpuclCreateContext)(const cl_context_properties*, cl_uint, const cl_device_id*, void (CL_CALLBACK *)(const char*, const void*, size_t, void*), void*, cl_int*);
cl_context (*cpuclCreateContextFromType)(const cl_context_properties*, cl_device_type, void (CL_CALLBACK *)(const char*, const void*, size_t, void*), void*, cl_int*);
cl_int (*cpuclGetContextInfo)(cl_context, cl_context_info, size_t, void*, size_t*);
cl_int (*cpuclRetainContext)(cl_context);
cl_int (*cpuclReleaseContext)(cl_context);
cl_int (*cpuclReleaseCommandQueue)(cl_command_queue);
cl_int (*cpuclReleaseMemObject)(cl_mem);
cl_int (*cpuclReleaseKernel)(cl_kernel);
cl_int (*cpuclReleaseProgram)(cl_program);
cl_program (*cpuclCreateProgramWithSource)(cl_context, cl_uint, const char**, const size_t*, cl_int*);
cl_int (*cpuclBuildProgram)(cl_program, cl_uint, const cl_device_id*, const char*, void (CL_CALLBACK *)(cl_program, void*), void*);
cl_int (*cpuclGetProgramBuildInfo)(cl_program, cl_device_id, cl_program_build_info, size_t, void*, size_t*);
cl_command_queue (*cpuclCreateCommandQueue)(cl_context, cl_device_id, cl_command_queue_properties, cl_int*);
cl_kernel (*cpuclCreateKernel)(cl_program, const char*, cl_int*);
cl_mem (*cpuclCreateBuffer)(cl_context, cl_mem_flags, size_t, void*, cl_int);
cl_int (*cpuclEnqueueWriteBuffer)(cl_command_queue, cl_mem, cl_bool, size_t, size_t, void*, cl_uint, const cl_event*, cl_event*);
cl_int (*cpuclWaitForEvents)(cl_uint, const cl_event*);
cl_int (*cpuclSetKernelArg)(cl_kernel, cl_uint, size_t*, const void*);
cl_int (*cpuclEnqueueNDRangeKernel)(cl_command_queue, cl_kernel, cl_uint, const size_t*, const size_t*, const size_t*, cl_uint, const cl_event*, cl_event*);
cl_int (*cpuclGetEventProfilingInfo)(cl_event, cl_profiling_info, size_t, void*, size_t*);

void POCL_Init();
void dlCheckError();
void *dlSym(void *handle, char *symbol);

void POCL_Init()
{
	void *hdlPOCL, *error;

	hdlPOCL = dlopen(CCL_PATH, RTLD_LAZY);
	if(!hdlPOCL){
		fprintf(stderr, "%s\n", dlerror());
		exit(1);
	}
	else printf("dlopen libPOCL.so successfully.\n");

	cpuclGetPlatformIDs	=	dlSym(hdlPOCL, "clGetPlatformIDs");
	cpuclGetPlatformInfo	= 	dlSym(hdlPOCL, "clGetPlatformInfo");
	cpuclGetDeviceIDs		= 	dlSym(hdlPOCL, "clGetDeviceIDs");
	cpuclGetDeviceInfo	=	dlSym(hdlPOCL, "clGetDeviceInfo");
	cpuclCreateContext	=	dlSym(hdlPOCL, "clCreateContext");
	cpuclCreateContextFromType	=	dlSym(hdlPOCL, "clCreateContextFromType");
	cpuclGetContextInfo	=	dlSym(hdlPOCL, "clGetContextInfo");
	cpuclRetainContext	=	dlSym(hdlPOCL, "clRetainContext");
	cpuclReleaseContext	=	dlSym(hdlPOCL, "clReleaseContext");
	cpuclReleaseCommandQueue	=	dlSym(hdlPOCL, "clReleaseCommandQueue");
	cpuclReleaseMemObject	=	dlSym(hdlPOCL, "clReleaseMemObject");
	cpuclReleaseKernel	=	dlSym(hdlPOCL, "clReleaseKernel");
	cpuclReleaseProgram	=	dlSym(hdlPOCL, "clReleaseProgram");
	cpuclCreateProgramWithSource	=	dlSym(hdlPOCL, "clCreateProgramWithSource");
	cpuclBuildProgram		=	dlSym(hdlPOCL, "clBuildProgram");
	cpuclGetProgramBuildInfo	=	dlSym(hdlPOCL, "clGetProgramBuildInfo");
	cpuclCreateCommandQueue	=	dlSym(hdlPOCL, "clCreateCommandQueue");
	cpuclCreateKernel		=	dlSym(hdlPOCL, "clCreateKernel");
	cpuclCreateBuffer		=	dlSym(hdlPOCL, "clCreateBuffer");
	cpuclEnqueueWriteBuffer	=	dlSym(hdlPOCL, "clEnqueueWriteBuffer");
	cpuclWaitForEvents	=	dlSym(hdlPOCL, "clWaitForEvents");
	cpuclSetKernelArg		=	dlSym(hdlPOCL, "clSetKernelArg");
	cpuclEnqueueNDRangeKernel	=	dlSym(hdlPOCL, "clEnqueueNDRangeKernel");
	cpuclGetEventProfilingInfo	=	dlSym(hdlPOCL, "clGetEventProfilingInfo");

	printf("CPU CL init done.\n");
}

void dlCheckError()
{
	char *error;
	if((error=dlerror())){
		fprintf(stderr, "%s\n", error);
		exit(1);
	}
}

void *dlSym(void *handle, char *symbol)
{
	void *temp = dlsym(handle, symbol);
	dlCheckError();
	return temp;
}
